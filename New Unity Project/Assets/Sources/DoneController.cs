﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DoneController : MonoBehaviour
{
    public GameObject DonePanel;
    public List<DinoEvents> Dinos;

    public void CheckDone()
    {
        int doneCount = 0;
        foreach (var dino in Dinos)
        {
            if (dino.Done)
            {
                doneCount++;
            }
        }

        if (doneCount >= Dinos.Count)
        {
            DonePanel.SetActive(true);
        }
    }
}
