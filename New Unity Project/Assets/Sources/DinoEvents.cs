﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class DinoEvents : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerClickHandler
{
    public Transform EtalonTransform;
    public DoneController checkDoneController;

    public bool Done = false;

    private Vector2 mouseShift;

    private void Start()
    {
        EtalonTransform.gameObject.SetActive(false);
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        var position = new Vector2(gameObject.transform.position.x, gameObject.transform.position.y);
        mouseShift = eventData.pressPosition - position;

        gameObject.transform.localScale = Vector3.one;
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (!Done)
        {
            transform.position = eventData.position - mouseShift;

            if (Vector2.Distance(eventData.position, EtalonTransform.position) < 50f)
            {
                gameObject.transform.position = EtalonTransform.position;
                Done = true;

                checkDoneController.CheckDone();
            }
        }
    }

    public void OnEndDrag(PointerEventData eventData) { }

    public void OnPointerClick(PointerEventData eventData)
    {
        Debug.Log(eventData.position);
    }
}
